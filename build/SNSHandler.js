"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = __importDefault(require("aws-sdk"));
var SNSHandler = /** @class */ (function () {
    function SNSHandler(awsCredentials) {
        this.createTopic = function (topicName, attributes, data) {
            var sns = new aws_sdk_1.default.SNS();
            var params = {
                Name: topicName
            };
            return new Promise(function (resolve, reject) {
                sns.createTopic(params, function (err, data) {
                    if (err) {
                        resolve({ statusCode: 500, Message: err });
                    }
                    else {
                        resolve({ statusCode: 200, Message: data.TopicArn });
                    }
                });
            });
        };
        aws_sdk_1.default.config.credentials = awsCredentials;
        aws_sdk_1.default.config.update({ region: awsCredentials.region });
    }
    return SNSHandler;
}());
exports.default = SNSHandler;
