"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = __importDefault(require("aws-sdk"));
var inquirer_1 = __importDefault(require("inquirer"));
var arg = process.argv.slice(2);
var args = {
    a: arg[0],
    s: arg[1],
    r: arg[2],
    b: arg[3]
};
var regions = [
    "us-east-1",
    "us-east-2",
    "us-west-1",
    "us-west-2",
    "ca-central-1",
    "eu-west-1",
    "eu-central-1",
    "eu-west-2",
    "ap-northeast-1",
    "ap-northeast-2",
    "ap-southeast-1",
    "ap-southeast-2",
    "ap-south-1",
    "sa-east-1"
];
inquirer_1.default
    .prompt([
    {
        type: "input",
        name: "accessKeyId",
        message: "AWS Access Key Id",
        when: function () {
            if (args.a) {
                return false;
            }
            return true;
        }
    },
    {
        type: "password",
        name: "secretAccessKey",
        message: "AWS Secret Access Key",
        when: function () {
            if (args.s) {
                return false;
            }
            return true;
        }
    },
    {
        type: "list",
        name: "region",
        message: "AWS Region",
        choices: regions,
        when: function () {
            if (args.r) {
                return false;
            }
            return true;
        }
    },
    {
        type: "input",
        name: "bucketName",
        message: "Name of the bucket",
        when: function () {
            if (args.b) {
                return false;
            }
            return true;
        }
    }
])
    .then(function (answers) {
    console.log(answers);
    console.log("args", args);
    mainScript(args.b ? args.b : answers.bucketName, {
        accessKeyId: args.a ? args.a : answers.accessKeyId,
        secretAccessKey: args.s ? args.s : answers.secretAccessKey,
        region: args.r ? args.r : answers.region
    });
});
function mainScript(bucketName, credentials) {
    return __awaiter(this, void 0, void 0, function () {
        var s3;
        return __generator(this, function (_a) {
            aws_sdk_1.default.config.credentials = credentials;
            aws_sdk_1.default.config.update({ region: credentials.region });
            console.log("createBucket AWS.config.credentials", aws_sdk_1.default.config.credentials);
            console.log("createBucket");
            s3 = new aws_sdk_1.default.S3();
            s3.createBucket({
                Bucket: bucketName
            }, function (err, data) {
                if (err) {
                    console.log(err, err.stack);
                }
                else {
                    console.log("Bucket created correctly", data);
                }
            });
            return [2 /*return*/];
        });
    });
}
exports.default = mainScript;
