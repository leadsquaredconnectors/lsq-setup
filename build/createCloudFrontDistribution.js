"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = __importDefault(require("aws-sdk"));
function default_1(bucketName, credentials) {
    return __awaiter(this, void 0, void 0, function () {
        var s3, cloudfront, domains, sslACM, params, _a, Id, DomainName, corsParams;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    aws_sdk_1.default.config.credentials = credentials;
                    aws_sdk_1.default.config.update({ region: credentials.region });
                    s3 = new aws_sdk_1.default.S3();
                    cloudfront = new aws_sdk_1.default.CloudFront();
                    domains = [];
                    sslACM = null;
                    params = {
                        DistributionConfig: {
                            Enabled: true,
                            CallerReference: String(Number(new Date())),
                            Comment: "Distribution for " + bucketName,
                            Aliases: {
                                Quantity: domains.length,
                                Items: domains
                            },
                            Origins: {
                                Quantity: 1,
                                Items: [
                                    {
                                        DomainName: bucketName + ".s3.amazonaws.com",
                                        Id: "S3-" + bucketName,
                                        S3OriginConfig: {
                                            OriginAccessIdentity: ""
                                        }
                                    }
                                ]
                            },
                            DefaultCacheBehavior: {
                                ForwardedValues: {
                                    Cookies: {
                                        Forward: "none"
                                    },
                                    QueryString: false
                                },
                                MinTTL: 0,
                                TargetOriginId: "S3-" + bucketName,
                                TrustedSigners: {
                                    Enabled: false,
                                    Quantity: 0
                                },
                                ViewerProtocolPolicy: "allow-all"
                            },
                            CustomErrorResponses: {
                                Quantity: 3,
                                Items: [
                                    {
                                        ErrorCode: 400,
                                        ErrorCachingMinTTL: 300,
                                        ResponseCode: "200",
                                        ResponsePagePath: "/index.html"
                                    },
                                    {
                                        ErrorCode: 403,
                                        ErrorCachingMinTTL: 300,
                                        ResponseCode: "200",
                                        ResponsePagePath: "/index.html"
                                    },
                                    {
                                        ErrorCode: 404,
                                        ErrorCachingMinTTL: 300,
                                        ResponseCode: "200",
                                        ResponsePagePath: "/index.html"
                                    }
                                ]
                            },
                            DefaultRootObject: "/index.html",
                            PriceClass: "PriceClass_All",
                            ViewerCertificate: sslACM
                                ? {
                                    ACMCertificateArn: sslACM,
                                    Certificate: "allow-all",
                                    CertificateSource: "acm",
                                    CloudFrontDefaultCertificate: false,
                                    MinimumProtocolVersion: "TLSv1",
                                    SSLSupportMethod: "sni-only"
                                }
                                : undefined
                        }
                    };
                    console.log("params", params);
                    return [4 /*yield*/, cloudfront.createDistribution(params).promise()];
                case 1:
                    _a = (_b.sent()).Distribution, Id = _a.Id, DomainName = _a.DomainName;
                    console.log("Id", Id, "DomainName", DomainName);
                    corsParams = {
                        Bucket: bucketName,
                        CORSConfiguration: {
                            CORSRules: [
                                {
                                    AllowedHeaders: ["*"],
                                    AllowedMethods: ["GET", "HEAD"],
                                    AllowedOrigins: ["*"]
                                }
                            ]
                        },
                        ContentMD5: ""
                    };
                    console.log("corsParams", corsParams);
                    s3.putBucketCors(corsParams, function (err, data) {
                        if (err)
                            console.log(err, err.stack);
                        else
                            console.log("putBucketCors", data); // successful response
                    });
                    console.log("CloudFront ready. Save the distribution id " + Id + " DomainName " + DomainName);
                    return [2 /*return*/, { Id: Id, DomainName: DomainName }];
            }
        });
    });
}
exports.default = default_1;
