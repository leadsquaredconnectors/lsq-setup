"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = __importDefault(require("aws-sdk"));
var inquirer_1 = __importDefault(require("inquirer"));
var appRoot = require("app-root-path");
var fs_1 = __importDefault(require("fs"));
var arg = process.argv.slice(2);
var args = {
    sc: arg[0],
    syml: arg[1],
    a: arg[2],
    s: arg[3],
    r: arg[4]
};
inquirer_1.default
    .prompt([
    {
        type: "input",
        name: "aws_access_key_id",
        message: "What's your aws_access_key_id?",
        when: function () {
            if (args.a) {
                return false;
            }
            return true;
        }
    },
    {
        type: "input",
        name: "aws_secret_access_key",
        message: "What's your aws_secret_access_key?",
        when: function () {
            if (args.s) {
                return false;
            }
            return true;
        }
    },
    {
        type: "input",
        name: "region",
        message: "What's your region?",
        when: function () {
            if (args.r) {
                return false;
            }
            return true;
        }
    }
])
    .then(function (answers) {
    console.log(answers);
    console.log("args", args);
    var credentials = {
        accessKeyId: args.a ? args.a : answers.aws_access_key_id,
        secretAccessKey: args.s ? args.s : answers.aws_secret_access_key,
        region: args.r ? args.r : answers.region
    };
    aws_sdk_1.default.config.credentials = credentials;
    aws_sdk_1.default.config.update({ region: args.r ? args.r : answers.region });
    console.log("config", aws_sdk_1.default.config.credentials);
    mainScript(appRoot + "/" + args.sc, appRoot + "/" + args.syml);
});
var removeDuplicates = function (test_list, item, index) {
    for (var i = index; i < test_list.length; i++) {
        if (test_list[i].TableName === item) {
            test_list.splice(i, 1);
            i--;
        }
    }
    return test_list;
};
var mainScript = function (sc, syml) { return __awaiter(void 0, void 0, void 0, function () {
    var scdata, symlData, dynamodbstreams;
    return __generator(this, function (_a) {
        scdata = require(sc);
        symlData = require(syml);
        dynamodbstreams = new aws_sdk_1.default.DynamoDBStreams();
        dynamodbstreams.listStreams({}, function (err, data) {
            if (err)
                console.log(err, err.stack);
            // an error occurred
            else {
                var dbStreams = JSON.parse(JSON.stringify(data.Streams)); // successful response
                console.log("dbStreams", dbStreams);
                for (var i = 0; i < dbStreams.length; i++) {
                    var obj = void 0;
                    for (var j = 0; j < scdata.length; j++) {
                        if (scdata[j].tableName === dbStreams[i].TableName) {
                            obj = scdata[j];
                            dbStreams = removeDuplicates(dbStreams, scdata[j].tableName, i + 1);
                        }
                    }
                    console.log("dbStreams after loop ", dbStreams);
                    //scdata.find(x => x.tableName === dbStreams[i].TableName);
                    console.log("obj", obj);
                    if (obj) {
                        var handlerResultObj = JSON.parse(JSON.stringify(obj.serverlessHandlerInfo.handlerObj));
                        console.log("handlerResultObj", handlerResultObj);
                        console.log("handlerResultObj =====>>>");
                        if (handlerResultObj["events"] &&
                            handlerResultObj["events"].find(function (x) { return Object.keys(x)[0] === "stream"; })) {
                            handlerResultObj["events"].find(function (x) { return Object.keys(x)[0] === "stream"; })["stream"].arn = dbStreams[i].StreamArn;
                        }
                        console.log("handlerResultObj", handlerResultObj);
                        symlData["ServerlessConfiguration"]["functions"][obj.serverlessHandlerInfo.handlerName] = handlerResultObj;
                        console.log("ServerlessConfiguration", JSON.stringify(symlData["ServerlessConfiguration"]["functions"][obj.serverlessHandlerInfo.handlerName]));
                    }
                }
                fs_1.default.writeFile(syml, JSON.stringify(symlData), function (err) {
                    if (err)
                        throw err;
                });
            }
        });
        return [2 /*return*/];
    });
}); };
