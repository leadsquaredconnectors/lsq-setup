interface AWSCredentials {
    accessKeyId: string;
    secretAccessKey: string;
    region?: string;
}
interface Attributes {
    DeliveryPolicy?: string;
    DisplayName?: string;
    FifoTopic?: boolean;
    Policy?: string;
    KmsMasterKeyId?: string;
    ContentBasedDeduplication?: boolean;
}
export default class SNSHandler {
    constructor(awsCredentials: AWSCredentials);
    createTopic: (topicName: string, attributes?: Attributes | undefined, data?: any[] | undefined) => Promise<unknown>;
}
export {};
