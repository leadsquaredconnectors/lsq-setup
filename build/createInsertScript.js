"use strict";
var jsonSql = require("json-sql")();
var uuid = require("uuid/v1");
var appRoot = require("app-root-path");
var arg = process.argv.slice(2);
var args = {
    sc: arg[0]
};
var getInsertScript = function (sc) {
    var scdata = require(sc);
    var connectorData = JSON.parse(JSON.stringify(scdata));
    console.log("connectorData", connectorData);
    var sql = jsonSql.build({
        type: "insert",
        table: "Connector_Base",
        values: {
            ConnectorId: uuid(),
            Category: connectorData.Category,
            NAME: connectorData.Name,
            Consumer: connectorData.Consumer,
            Configuration: JSON.stringify(connectorData.Configuration),
            MarketPlaceDisplayConfiguration: JSON.stringify(connectorData.MarketPlaceDisplayConfig),
            CallBackUrlConfiguration: JSON.stringify(connectorData.CallBackUrlConfig),
            InstalledAppHooks: null,
            ApplicationMenuHooks: JSON.stringify(connectorData.ApplicationMenuHooksConfig),
            ActionHooks: JSON.stringify(connectorData.LeadMenuActionConfig),
            AutomationConfiguration: JSON.stringify(connectorData.AutomationMenuConfig),
            LeadDetailsTabConfiguration: connectorData.LeadDetailsTabConfiguration
                ? JSON.stringify(connectorData.LeadDetailsTabConfiguration)
                : null,
            MobileAppConfiguration: connectorData.MobileAppConfiguration
                ? JSON.stringify(connectorData.MobileAppConfiguration)
                : null,
            AccessibilityConfiguration: connectorData.AccessibilityConfiguration
                ? JSON.stringify(connectorData.AccessibilityConfiguration)
                : null,
            StatusCode: "0",
            StatusReason: "0",
            CreatedBy: "LeadSquared",
            ModifiedBy: "LeadSquared",
            CreatedOn: new Date(),
            ModifiedOn: new Date(),
            IsMenuLinked: 1
        }
    });
    console.log(sql);
    var query = sql.query;
    query = query.replace(/\"/g, "");
    var values = sql.values;
    var keys = Object.keys(values);
    var keyValue = Object.values(values);
    for (var i = 0; i < keys.length; i++) {
        var str = "$" + keys[i];
        console.log("str", str);
        query = query.replace(str, JSON.stringify(keyValue[i]));
    }
    return query;
};
var data = getInsertScript(appRoot + "/" + args.sc);
console.log("ScriptData -------------------->", data);
