import config from "../../config.json";
import fs from "fs";
import path from "path";

const __dirname = path.resolve();

const register = () => {
    let connectorObj = config.Connector;
    let results = JSON.stringify(connectorObj);
    const environment =
        process.argv.length > 2 && process.argv[process.argv.length - 1]
            ? process.argv[process.argv.length - 1]
            : "dev";
    let exp = new RegExp(/\${[^\}]*}/g);
    JSON.stringify(connectorObj)
        .match(exp)
        .map((placeholder) => {
            results = results.replace(
                placeholder,
                config[placeholder.split(".")[0].substring(2)][environment][
                    placeholder.split(".")[1].slice(0, -1)
                ]
            );
        });
    console.log("generated file under root");
    fs.writeFile(path.join(__dirname, "/connector.json"), results, (err) => {
        if (err) throw err;
    });
    console.log("generated file under api");
    fs.writeFile(path.join(__dirname, "api/connector.json"), results, (err) => {
        if (err) throw err;
    });
};
(() => {
    console.log("starting register...");
    register();
    console.log("finished registering...");
})();
