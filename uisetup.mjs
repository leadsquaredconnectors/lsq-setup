import fs from "fs";
import config from "../../config.json";
import path from "path";

const generateENV = (environment) => {
    const filePath = path.join(path.resolve(), "/ui");
    Object.keys(config.UI_Variables).map((key) => {
        if (key === environment) {
            fs.writeFileSync(
                `${filePath}/.env`,
                formatToKeyValue(
                    JSON.parse(replacePlaceholders(config.UI_Variables[key], environment))
                ),
                (err) => {
                    if (err) throw err;
                }
            );
        }
    });
};

const formatToKeyValue = (data) => {
    return Object.keys(data)
        .map((key) => {
            return `${key}=${data[key]}`;
        })
        .splice(",")
        .join("\n");
};

const replacePlaceholders = (uiVariables, environment) => {
    let results = JSON.stringify(uiVariables);
    let exp = new RegExp(/\${[^\}]*}/g);
    JSON.stringify(uiVariables)
        .match(exp)
        .map((placeholder) => {
            if (placeholder.indexOf(".") >= 0) {
                results = results.replace(
                    placeholder,
                    config[placeholder.split(".")[0].substring(2)][environment][
                    placeholder.split(".")[1].slice(0, -1)
                    ]
                );
            } else {
                results = results.replace(
                    placeholder,
                    config[placeholder.substring(2, placeholder.length - 1)]
                );
            }
        });
    return results;
};

(() => {
    const environment =
        process.argv.length > 2 && process.argv[process.argv.length - 1]
            ? process.argv[process.argv.length - 1]
            : "dev";
    console.log("generating .env file location:@root/ui for environment:", environment);
    generateENV(environment);
    console.log("finished generating file");
})();
