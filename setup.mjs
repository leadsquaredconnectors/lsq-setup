import fs from "fs";
import yaml from "json2yaml";
import config from "../../config.json";
import path from "path";
const __dirname = path.resolve();

async function generateFiles() {
    const environment =
        process.argv.length > 2 && process.argv[process.argv.length - 1]
            ? process.argv[process.argv.length - 1]
            : "dev";
    console.log("creating for env", environment);
    generateAPIEnvironmentFiles(environment);
    await generateServerlessFile(environment);
    readServerlessFile();
    register(environment);
}
const replacePlaceholders = (uiVariables, environment) => {
    let results = JSON.stringify(uiVariables);
    let exp = new RegExp(/\${[^\}]*}/g);
    JSON.stringify(uiVariables)
        .match(exp)
        .map((placeholder) => {
            console.log("Placeholders", placeholder)
            if (placeholder.indexOf(".") >= 0) {
                let placeKey = config[placeholder.split(".")[0].substring(2)];
                results = results.replace(
                    placeholder,
                    placeKey && placeKey[environment] ? placeKey[environment][placeholder.split(".")[1].slice(0, -1)] : placeholder
                );
            } else {
                results = results.replace(
                    placeholder,
                    config[placeholder.substring(2, placeholder.length - 1)] || placeholder
                );
            }
        });
    return results;
};

const generateAPIEnvironmentFiles = (environment) => {
    fs.writeFile(
        path.join(__dirname, "/api/env.yml"),
        yaml.stringify(JSON.parse(replacePlaceholders(config.API_Variables, environment))),
        (err) => {
            if (err) throw err;
        }
    );
};

const generateServerlessFile = (environment) => {
    let serverlessConfiguration = JSON.parse(replacePlaceholders(config.ServerlessConfiguration, environment));
    fs.writeFile(
        path.join(__dirname, "/api/serverless.yml"),
        yaml.stringify(serverlessConfiguration),
        (err) => {
            if (err) throw err;
        }
    );
};

const readServerlessFile = () => {
    const updateServerlessFile = (content) => {
        console.log(content);
        fs.writeFile(path.join(__dirname, "/api/serverless.yml"), content, (err) => {
            if (err) throw err;
        });
    };
    let content;
    fs.readFile(path.join(__dirname, "/api/serverless.yml"), "utf8", function read(err, data) {
        if (err) {
            throw err;
        }
        content = data;
        content = content.replace(/"/g, "");
        updateServerlessFile(content);
    });
};

const register = (environment) => {
    if (Object.keys(config.Connector).length) {
        console.log('generated file under api');
        fs.writeFile(path.join(__dirname, 'api/connector.json'), replacePlaceholders(config.Connector, environment), (err) => {
            if (err) throw err;
        });
    }
}

(() => {
    console.log("starting setup...");
    generateFiles();
    console.log("finished setting-up...");
})();
